jQuery(document).ready(function ($) {
    $('.hero').slick({
        dots: true,
        infinite: true,
        speed: 500,
        fade: !0,
        cssEase: 'linear',
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 1000,
        draggable: true,
        arrows: false,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true
                }
            },
            {
                breakpoint: 768,
                settings: {
                    draggable: true,
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    draggable: true,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    draggable: true,
                    slidesToScroll: 1
                }
            }

        ]
    });
});

var swiper = new Swiper(".mySwiper", {
    loop: true,
    slidesPerView: "auto",
    spaceBetween: 30,
    breakpoints: {
        0: {
            slidesPerView: "auto"
        },
        992: {
            slidesPerView: 2
        },
    },
    navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
    },
});


function increaseCookieValue(cookieName, cookieValue) {
    // Kiểm tra xem cookie có tồn tại hay không
    var existingCookie = Cookies.get(cookieName);
    
    if (existingCookie) {
      // Cookie đã tồn tại, thực hiện cập nhật giá trị
      Cookies.set(cookieName, cookieValue, { expires: 700, path: '/' });
    } else {
      // Cookie chưa tồn tại, tạo mới cookie
      console.log("OKE");
      Cookies.set(cookieName, cookieValue, { expires: 700, path: '/' });
    }
  }
  

  // Xử lý sự kiện khi nhấn vào nút "Đặt phòng"
var button = document.getElementById("dat-phong-button"); // Thay "dat-phong-button" bằng ID thật của nút đặt phòng
  

// Lấy giá trị count từ Local Storage (nếu đã tồn tại)
var count = localStorage.getItem("count");
count = count ? parseInt(count) : 0; // Chuyển đổi sang số nguyên

// Lấy giá trị obj từ Local Storage (nếu đã tồn tại)
var obj = localStorage.getItem("obj");
obj = obj ? JSON.parse(obj) : {};



button.addEventListener("click", function() {
    var cookieValue = document.getElementById("data").value;
    var data = JSON.parse(cookieValue);
    
    var id = data.id;
    var price = data.price;

    if (obj.hasOwnProperty(id)) {
        obj[id].quantity = obj[id].quantity + 1;
    } else {
        obj[id] = {
            id: id,
            price: price,
            quantity: 1
        };
    }
    count++;

    dataArray = JSON.stringify(obj);    
    increaseCookieValue("thong-tin-dat-phong", dataArray); 

    localStorage.setItem("count", count.toString());
    localStorage.setItem("obj", JSON.stringify(obj));

    alert("Thêm vào giỏ hàng thành công");
    window.location.reload();
});

// ...


console.log(obj);




  



