let memberTabItem = document.querySelectorAll('.page-tab .tab-item');
let memberTabItemList = document.querySelectorAll('.page-tab .has-child-dropdown');
let memberTabContent = document.querySelectorAll('.page-tab .tab-content');
for (let i = 0; i < memberTabItem.length; i++) {
    memberTabItem[i].addEventListener('click', function () {
        for (let i = 0; i < memberTabItem.length; i++) {
            memberTabItem[i].classList.remove('active');
            if (document.getElementById("tab-list")) {
                document.getElementById("tab-list").style.transition = "opacity 0s ";
                document.getElementById("tab-list").style.opacity = 0;
            }

        }
        for (let i = 0; i < memberTabItemList.length; i++) {
            memberTabItemList[i].classList.remove('active');
        }
        let target = this.dataset.target;
        this.classList.add('active');
        let parentWithClass = this.closest('.tab-item-title');
        if (parentWithClass){
            parentWithClass.classList.add('active');
        }
        for (let j = 0; j < memberTabContent.length; j++) {
            memberTabContent[j].classList.remove('show');
            if (document.getElementById("tab-list")) {
                setTimeout(function () {
                    document.getElementById("tab-list").style.transition = "opacity 1s ";
                    document.getElementById("tab-list").style.opacity = 1;
                },500);
            }

        }
        document.querySelector('.page-tab #' + target).classList.add('show');
    })
}