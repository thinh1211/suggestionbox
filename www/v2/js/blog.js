let memberTabItem = document.querySelectorAll('.page-tab .tab-item');
let memberTabItemList = document.querySelectorAll('.page-tab .has-child-dropdown');
let memberTabContent = document.querySelectorAll('.page-tab .tab-content');
for (let i = 0; i < memberTabItem.length; i++) {
    memberTabItem[i].addEventListener('click', function () {
        for (let i = 0; i < memberTabItem.length; i++) {
            memberTabItem[i].classList.remove('active');
            if (document.getElementById("tab-list")) {
                document.getElementById("tab-list").style.transition = "opacity 0s ";
                document.getElementById("tab-list").style.opacity = 0;
            }

        }
        for (let i = 0; i < memberTabItemList.length; i++) {
            memberTabItemList[i].classList.remove('active');
        }
        let target = this.dataset.target;
        this.classList.add('active');
        let parentWithClass = this.closest('.tab-item-title');
        if (parentWithClass){
            parentWithClass.classList.add('active');
        }
        for (let j = 0; j < memberTabContent.length; j++) {
            memberTabContent[j].classList.remove('show');
            if (document.getElementById("tab-list")) {
                setTimeout(function () {
                    document.getElementById("tab-list").style.transition = "opacity 1s ";
                    document.getElementById("tab-list").style.opacity = 1;
                },500);
            }

        }
        document.querySelector('.page-tab #' + target).classList.add('show');
    })
}
// const unidecode = require('unidecode');

function ajax(obj) {
    if (isFunction(obj.before)) {
        obj.before();
    }
    var xhr = new XMLHttpRequest();
    var type = obj.type.toLowerCase();
    var data = null;
    if (obj.data) {
        data = params(obj.data);
    }
    if (type=='get') {
        xhr.open('GET', obj.url);
    }
    if (type=='post') {
        xhr.open('POST', obj.url);
        if (obj.processForm == true) {
            var data = obj.data;
        }
        else {
            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        }
    }
    xhr.onload = function() {
        if (xhr.status === 200) {
            obj.success(xhr.response);
        }
        else if (xhr.status !== 200) {
            if (undefined !== obj.error) {
                obj.error();
            }
        }
    };
    if (obj.data) {
        // console.log(JSON.stringify(obj.data));
        xhr.send(data);
    }
    else {
        xhr.send();
    }
}

    // function toggleSearchForm() {
    //     var searchForm = document.getElementById("search-form");
    //     searchForm.style.display = (searchForm.style.display === "none") ? "block" : "none";
    // }

    function submitSearchForm(event) {
    event.preventDefault(); 
    var searchInput = document.getElementById("search-input").value;

    if (searchInput) {
        var currentUrl = "http://newcentury.xyz/news/"; 
        var separator = (currentUrl.includes('?')) ? '&' : '?'; // Xác định dấu phân tách thích hợp

        // Cập nhật URL của trang với tham số searchInput
        var newUrl = currentUrl + separator + 'searchInput=' + encodeURIComponent(searchInput);
        window.location.href = newUrl;
    } else if (searchInput === ""){

        var newUrl = "http://newcentury.xyz/news"
        window.location.href = newUrl;
    }
    }


    function displaySearchResults(results) {
        var searchResults = document.getElementById("test");
        searchResults.innerHTML = "";

        //#region test
        var dropdownDiv = document.createElement("div");
        dropdownDiv.classList.add("search-dropdown");
        var i = 0;
        
            results[0].forEach(function(obj, index) {
                if ( i < 5) {
                    var linkItem = document.createElement("a");
                    linkItem.innerText = obj.title;
                    linkItem.style.backgroundColor = "#41424A";
                    linkItem.style.display = "flex";
                    linkItem.style.alignItems = "center";
                    linkItem.style.justifyContent = "space-between";
                    linkItem.style.cursor = "pointer";
                    
                    var image = document.createElement("img");
                    image.src = "http://newcentury.xyz/" + obj.images;
                    image.style.width = "70px";
                    image.style.height = "70px";
                    image.style.marginRight = "10px";

                    linkItem.insertBefore(image, linkItem.firstChild);
                    linkItem.style.color = "white";
                    
                    linkItem.classList.add("link");
                    image.classList.add("img");

                    linkItem.addEventListener("click", function () {
                        var slugs = obj.slug;
                        var ids = obj.id;
                        window.location.href = "/news/" + slugs + "."+ ids +  ".html";
                    });

                    dropdownDiv.appendChild(linkItem);
                    i++;
                } 
            });
        if (i > 4) {
            var seeMore = document.createElement("a");
            seeMore.innerText = "Xem thêm";
            seeMore.addEventListener("click", submitSearchForm);
            seeMore.classList.add("centered-text");
            seeMore.style.cursor = "pointer";
            dropdownDiv.appendChild(seeMore);
        }
        searchResults.appendChild(dropdownDiv);
        //#endregion test

    }

    
    function searchAndDisplayResults(obj) {
        var search_input = obj.value;
        console.log(search_input);
        ajax({
            url: '/news/ajax/ajax-searching',
            type: 'POST',
            data: {search: search_input},
            success: function(response) {
                var results = JSON.parse(response); // nhận thành công rồi

                // Object.keys(results).forEach(key => {
                //     console.log(key)
                //   ;                        // the name of the current key.
                //     console.log(results[0][key]);   // the value of the current key.
                //   });
                // var keys = Object.keys(results);
                // var myArray = keys.map(function(key) {
                //     return results[key];
                // });
                //   return;
                // console.log(results);
                // return;
                displaySearchResults(results);
            },
            error: function() {
                console.error('Lỗi khi tìm kiếm tin tức');
            }
        });
    }
    

    // Xử lý sự kiện khi nhập giá trị vào ô tìm kiếm
    function handleSearchInput(searchInput) {
        var searchResults = document.getElementById("search-results");
        searchResults.innerHTML = "";

        if (searchInput === "") {
            return;
        }

        searchAndDisplayResults(searchInput);
    }



    function isFunction(possibleFunction) {
        return typeof(possibleFunction) === typeof(Function);
    }
    function params(object) {
        var encodedString = '';
        for (var prop in object) {
            if (object.hasOwnProperty(prop)) {
                if (encodedString.length > 0) {
                    encodedString += '&';
                }
                encodedString += encodeURI(prop + '=' + object[prop]);
            }
        }
        return encodedString;
    }



    