jQuery(document).ready(function ($) {
    $('.hero').slick({
        dots: true,
        infinite: true,
        speed: 500,
        fade: !0,
        cssEase: 'linear',
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 8000,
        arrows: false,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    // slidesToShow: 1,
                    // slidesToScroll: 1,
                    // infinite: true,
                    draggable: true,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    draggable: true,
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    draggable: true,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    draggable: true,
                    slidesToScroll: 1
                }
            }

        ]
    });
});

var swiper = new Swiper(".mySwiper", {
    loop: true,
    slidesPerView: 3,
    spaceBetween: 30,
    breakpoints: {
        0: {
            slidesPerView: 1
        },
        992: {
            slidesPerView: 3
        },
    },
});
var swiper = new Swiper(".mySwiperAmenities", {
    loop: true,
    slidesPerView: 3,
    spaceBetween: 30,
    breakpoints: {
        0: {
            slidesPerView: 1
        },
        992: {
            slidesPerView: 3
        },
    },
});