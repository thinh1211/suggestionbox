function isFunction(possibleFunction) {
    return typeof(possibleFunction) === typeof(Function);
}
function params(object) {
    var encodedString = '';
    for (var prop in object) {
        if (object.hasOwnProperty(prop)) {
            if (encodedString.length > 0) {
                encodedString += '&';
            }
            encodedString += encodeURI(prop + '=' + object[prop]);
        }
    }
    return encodedString;
}

function ajax(obj) {
    if (isFunction(obj.before)) {
        obj.before();
    }
    var xhr = new XMLHttpRequest();
    var type = obj.type.toLowerCase();
    var data = null;
    if (obj.data) {
        data = params(obj.data);
    }
    if (type=='get') {
        xhr.open('GET', obj.url);
    }
    if (type=='post') {
        xhr.open('POST', obj.url);
        if (obj.processForm == true) {
            var data = obj.data;
        }
        else {
            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        }
    }
    xhr.onload = function() {
        if (xhr.status === 200) {
            obj.success(xhr.response);
        }
        else if (xhr.status !== 200) {
            if (undefined !== obj.error) {
                obj.error();
            }
        }
    };
    if (obj.data) {
        // alert(obj.data);
        xhr.send(data);
    }
    else {
        xhr.send();
    }
}

  function clearLocalStorageConfirmation() {
        var Cookies = document.cookie.split(';');
         // set 1 Jan, 1970 expiry for every cookies
        for (var i = 0; i < Cookies.length; i++)
        document.cookie = Cookies[i] + "=;expires=" + new Date(0).toUTCString();

         //cập nhật Obj
         localStorage.clear();

        window.location.reload();
        //  document.cookie = cookie.replace("=","=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/");
        
  }
  var objData = localStorage.getItem("obj");
  var obj = JSON.parse(objData);

  function deleteRow(id) {
    var cookie = Cookies.get("thong-tin-dat-phong");
    var decodedData = decodeURIComponent(cookie);
    var arrayData = JSON.parse(decodedData);
    console.log(arrayData);
    // xóa id trong cookie
    for (var key in arrayData) {
        if (arrayData.hasOwnProperty(key)) {
          var value = arrayData[key];
          var idCheck = value.id;
          if (idCheck == id) {
            delete arrayData[key];
          }          
        }
      }    
    arrayData = JSON.stringify(arrayData);    
    Cookies.set("thong-tin-dat-phong", arrayData);  

    // Cập nhật cookie và localStorage   
    localStorage.setItem("obj", arrayData);
    window.location.reload();
}
  
for (var key in obj) {
    if (obj.hasOwnProperty(key)) {
      var value = obj[key];
      console.log(key + ": " + value);
    }
  }

  window.onload = function() {
    

        // submitBtn.style.display = 'none';
    // Lấy thẻ <thead> bằng cách sử dụng querySelector
    var tableHeader = document.querySelector('#tableCustomer thead');
    
    // Lấy danh sách ô thông tin khách hàng bằng cách sử dụng querySelectorAll
    var customerInfoRows = document.querySelectorAll('.customer-info-row');
    
    // Gắn sự kiện click vào ô thông tin khách hàng
    tableHeader.addEventListener('click', function() {
        
        // Lặp qua danh sách ô thông tin khách hàng
        for (var i = 0; i < customerInfoRows.length; i++) {
            var row = customerInfoRows[i];
            
            // Kiểm tra trạng thái hiện tại của ô thông tin khách hàng
            var display = row.style.display;
            
            // Thay đổi trạng thái hiển thị (ẩn hoặc hiện) của ô thông tin khách hàng
            row.style.display = (display === 'none' || display === '') ? 'table-row' : 'none';
        }
    });
    
};


// nhất nút đặt hàng
function submitCustomerInfo() {
    var fullName = document.getElementById('inputCustomerName').value;
    var phone = document.getElementById('inputPhone').value;

    document.getElementById('inputCustomerName').value = '';
    document.getElementById('inputPhone').value = '';
}

function checkOut(obj){
    window.event.preventDefault();
    button = obj.querySelector('.btn-add');
    var formData = new FormData(obj);
    ajax({
        url: '/news/ajax/shopping',
        type: 'POST',
        data: formData,
        before: function() {
            // button.setAttribute('disabled', true);
            // loadingCart();
        },
        // nhảy vào được success 
        success: function (response) {
            // alert(response);
            var data = JSON.parse(response);

            clearLocalStorageConfirmation();
            var cookies = document.cookie.split(";");0
            // xóa hết cookie
            for (var i = 0; i < cookies.length; i++) {
                var cookie = cookies[i];
                var eqPos = cookie.indexOf("=");
                var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
                document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 UTC;path=/;";
            }

            if (data.msg != '') {
                alert(data.msg);
            }
            if (data.field) {
                var field = obj.querySelector("#"+data.field);
                if (field) {
                    field.focus();
                }
            }
            else {
                if (data.reloads) {
                    window.location.href = data.url;
                    // window.location.reload();
                }
                if(data.url){
                    window.location.href = data.url;
                }
                
            }
            // loadingCart(false);
        },
        error: function () {
            alert('Lỗi hệ thống! Vui lòng thử lại sau.');
            // loadingCart(false);
        },
        processForm: true,
    });
    
    
}


    // check đặt hàng
    var localStorageKeys = Object.keys(localStorage);
    if (localStorageKeys.length > 0) {
        // Hiển thị table bằng cách đặt thuộc tính display thành block
        document.getElementById('tableCustomer').style.display = 'block';
    }

    

  function updateCreateCookieValue(cookieName, cookieValue, id) {
    // Kiểm tra xem cookie có tồn tại hay không
    var existingCookie = Cookies.get(cookieName);
    
    if (existingCookie) {
        // Cookie đã tồn tại, thực hiện cập nhật giá trị
        var existingObjStorage = JSON.parse(existingCookie);
        
        for (var id in cookieValue) {
            console.log(existingObjStorage[id].quantity);
            console.log(cookieValue[id].quantity);
            existingObjStorage[id].quantity = cookieValue[id].quantity;
        }
        Cookies.set(cookieName, JSON.stringify(existingObjStorage), { expires: 700, path: '/' });
    } else {
        // Cookie chưa tồn tại, tạo mới cookie
        Cookies.set(cookieName, JSON.stringify(cookieValue), { expires: 700, path: '/' });
    }
  }
  
var ObjStorage = localStorage.getItem("obj");
ObjStorage = ObjStorage ? JSON.parse(ObjStorage) : {};

function increaseCookieValue(cookieName, objStorage, id) {
    var existingCookie = Cookies.get(cookieName);
    var updatedObjStorage = objStorage;
    
    if (existingCookie) {
      var existingObjStorage = JSON.parse(existingCookie);
      
      if (existingObjStorage.hasOwnProperty(id)) {
        existingObjStorage[id].quantity += 1;
      } else {
        existingObjStorage[id] = {
          id: id,
          quantity: 1
        };
      }
      
      updatedObjStorage = existingObjStorage;
    } else {
      updatedObjStorage[id] = {
        id: id,
        quantity: 1
      };
    }

    Cookies.set(cookieName, JSON.stringify(updatedObjStorage), { expires: 700, path: '/' });
  }
  
  function incrementCount(element, id) {
    if (ObjStorage.hasOwnProperty(id)) {
      ObjStorage[id].quantity = ObjStorage[id].quantity + 1;
    } 
    
    increaseCookieValue("thong-tin-dat-phong", ObjStorage, id); 
    window.location.reload();
  }
  

  function decreaseCookieValue(cookieName, objStorage, id) {
    var existingCookie = Cookies.get(cookieName);
    var updatedObjStorage = objStorage;
    
    if (existingCookie) {
      var existingObjStorage = JSON.parse(existingCookie);
      
      if (existingObjStorage.hasOwnProperty(id) && existingObjStorage[id].quantity > 1) {
        existingObjStorage[id].quantity -= 1;
      } else {
        existingObjStorage[id] = {
          id: id,
          quantity: 1
        };
      }
      
      updatedObjStorage = existingObjStorage;
    } else {
      updatedObjStorage[id] = {
        id: id,
        quantity: 1
      };
    }

    Cookies.set(cookieName, JSON.stringify(updatedObjStorage), { expires: 700, path: '/' });
  }

function decrementCount(element, id) {
    if (ObjStorage.hasOwnProperty(id)) {
        if (ObjStorage[id].quantity > 1) {
            ObjStorage[id].quantity = ObjStorage[id].quantity - 1;
            decreaseCookieValue("thong-tin-dat-phong", ObjStorage, id); 
            window.location.reload();
        }
      } 
      
      
}

// function updateValues(countElement,count, price, action ) {
//     var rowTotalElement = countElement.parentNode.nextElementSibling;


//     var xhr = new XMLHttpRequest();
//     xhr.open('POST', '/news/ajax/increase-decrease', true);
//     xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
//     xhr.onreadystatechange = function() {
//         if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
//             alert(this.response);
//             // var response = JSON.parse(xhr.responseText);

//             if (response.success) {
//                 // Cập nhật giá trị rowTotal
//                 // rowTotalElement.textContent = response.rowTotal + ' VNĐ';
                
//                 // // Cập nhật giá trị total
//                 // totalElement.textContent = response.total + ' VNĐ';
//                 // setCookie('total', response.total, 1); // Cập nhật cookie total
//             } else {
//                 // Xử lý lỗi nếu cần thiết
//             }
//         }
//     };
//     var formData =  '&price=' + encodeURIComponent(price)+  '&action=' + encodeURIComponent(action);
//     alert(encodeURIComponent(action));
//     xhr.send(formData);
// }
