<?php

class Business_Addon_Hnproducts extends Business_Abstract
{
    private $_tablename = 'hn_products';
    private $_prefix_cache = 'Business_Addon_Hnproducts::';
    private static $_instance = null;

    function __construct(){
    }

    public static function getInstance() {
        if(self::$_instance == null) {
            self::$_instance = new Business_Addon_Hnproducts();
        }
        return self::$_instance;
    }

    function getDbConnection() {
        $db = Globals::getDbConnection('maindb', false);
        return $db;
    }

    public function getList($itemid) {
        $cache = GlobalCache::getCacheInstance('ws');
        $cacheKey = md5($this->_prefix_cache . "getList-{$itemid}");
        $result = $cache->getCache($cacheKey);
        if ($result) {
            return $result;
        }

        $db = $this->getDbConnection();
        $query = "SELECT * FROM {$this->_tablename} WHERE itemid = {$itemid}";

        $result = $db->fetchAll($query);
        $cache->setCache($cacheKey,$result);
        return $result;
    }

    public function callGetRandomObjectsByThuongHieuChipPin($itemid1, $itemid2, $itemid3, $itemid4, $itemid5) {
        $db = $this->getDbConnection();
        $query = "SELECT GetRandomObjectsByThuongHieuChipPin($itemid1, $itemid2, $itemid3, $itemid4, $itemid5) AS randomObjects";
        
        $result = $db->fetchAll($query);
        return $result;
    }
    
    public function insert($data) {
        $db = $this->getDbConnection();
        $result = $db->insert($this->_tablename,$data);
        if ($result > 0) {
            $lastid= $db->lastInsertId($this->_tablename);
        }
        return $lastid;
    }

}