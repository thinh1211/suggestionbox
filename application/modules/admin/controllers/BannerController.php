<?php
/* TODO: Add code here */
class Admin_BannerController extends Zend_Controller_Action
{
    private $menu = 'menu_news';
    private $_identity;
    private $_bannersList = array(
        "1"=> "Trang chủ - Banner chính",
        "2"=> "Trang giới thiệu",
    );
    public function init()
    {
        ini_set('display_errors', '1');
        BlockManager::setLayout('hnamtemplatecontent');
        $auth = Zend_Auth::getInstance();
        $identity = $auth->getIdentity();
        if(!is_null($identity) and count($identity) != 0) {
            $fullname = $identity->fullname?$identity->fullname:$identity->username;
            $this->view->fullname = $fullname;
        }else{
            $this->_redirect('/admin/home/login');
        }
        $this->_identity = (array) $auth->getIdentity();
        $this->view->menu_active = "banner";
    }

    public function indexAction() {
        $data_pageshow= $this->_bannersList;
        $this->view->data_pageshow=$data_pageshow;
    }
    public function editAction() {
        $this->view->menu_sub_active = "list_banner";
        $this->view->inlineScript()->appendFile("/admin/js/banner.js?v=".Globals::getVersion());
        $id = (int)$this->_request->getParam("id");
        $detail = array();
        if ($id){
            $__banner = Business_Addon_Banner::getInstance();
            $detail = $__banner->getDetail($id);
        }
        $this->view->detail = $detail;
        $data_pageshow= $this->_bannersList;
        $this->view->data_pageshow=$data_pageshow;
        $this->view->token = Business_Addon_General::getInstance()->getToken();
    }

    public function listBannerAction(){
        $this->view->menu_sub_active = "list_banner";
//        css datatables
        $this->view->headLink()->appendStylesheet("/admin/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css");
        $this->view->headLink()->appendStylesheet("/admin/plugins/datatables-responsive/css/responsive.bootstrap4.min.css");
        $this->view->headLink()->appendStylesheet("/admin/plugins/datatables-buttons/css/buttons.bootstrap4.min.css");
//js datatables
        $this->view->inlineScript()->appendFile("/admin/plugins/datatables/jquery.dataTables.min.js");
        $this->view->inlineScript()->appendFile("/admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js");
        $this->view->inlineScript()->appendFile("/admin/plugins/datatables-responsive/js/dataTables.responsive.min.js");
        $this->view->inlineScript()->appendFile("/admin/plugins/datatables-responsive/js/responsive.bootstrap4.min.js");
        $this->view->inlineScript()->appendFile("/admin/plugins/datatables-buttons/js/dataTables.buttons.min.js");
        $this->view->inlineScript()->appendFile("/admin/plugins/datatables-buttons/js/buttons.bootstrap4.min.js");
        $this->view->inlineScript()->appendFile("/admin/plugins/jszip/jszip.min.js");
        $this->view->inlineScript()->appendFile("/admin/plugins/pdfmake/pdfmake.min.js");
        $this->view->inlineScript()->appendFile("/admin/plugins/pdfmake/vfs_fonts.js");
        $this->view->inlineScript()->appendFile("/admin/plugins/datatables-buttons/js/buttons.html5.min.js");
        $this->view->inlineScript()->appendFile("/admin/plugins/datatables-buttons/js/buttons.print.min.js");
        $this->view->inlineScript()->appendFile("/admin/plugins/datatables-buttons/js/buttons.colVis.min.js");
        $this->view->inlineScript()->appendFile("/admin/js/banner.js?v=".Globals::getVersion());
        $data_pageshow= $this->_bannersList;
        $this->view->data_pageshow=$data_pageshow;
    }

    public function changeStatusBannerAction(){
        $this->_helper->Layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        if(!is_null($this->_identity) and count($this->_identity) != 0) {
            $id  = $this->_request->getParam('id');
            $type  = (int)$this->_request->getParam('type');
            $token  = $this->_request->getParam('token');
            $__ztoken = md5("NewCenruryAbcdqwerBanner".$id);
            if ($token != $__ztoken){
                echo json_encode(array('msg' => "Dữ liệu không đúng. Vui lòng thử lại",'reloads' => true));
                die();
            }

            $detail_banner = Business_Addon_Banner::getInstance()->getDetail($id);
            if (!$detail_banner){
                echo json_encode(array('msg' => "Dữ liệu không đúng. Vui lòng thử lại",'reloads' => true));
                die();
            }

            $data_update = array(
                "userid_update"=> $this->_identity['userid'],
                "enabled"=> $type,
            );

            try{
                Business_Addon_General::getInstance()->updateDB('addon_banner',$data_update,'id='.$id);
                echo json_encode(array('msg' => "Cập nhật thành công"));
                die();
            }catch (Exception $e){
                echo json_encode(array('msg' => "Có lỗi xảy ra. Vui lòng thử lại",'reloads' => true));
                die();
            }

        }else{
            echo json_encode(array('msg' => "Vui lòng đăng nhập",'redirect' => '/admin/home/login'));
            die();
        }
    }

    public function ajaxListBannerAction(){
        $this->_helper->Layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $_general = Business_Addon_General::getInstance();
        $draw = 0;
        $row = 0;
        if (isset($_POST['start'])){
            $row = $_POST['start'];
        }
        if (isset($_POST['draw'])){
            $draw = $_POST['draw'];
        }

        if (isset($_POST['length'])){
            $rowperpage = $_POST['length']; // Rows display per page
        }
        $columnIndex = 0;
        $columnName = "";
        if (isset($_POST['order'][0]['column'])){
            $columnIndex = $_POST['order'][0]['column']; // Column index
            $columnName = $_POST['columns'][$columnIndex]['data']; // Column name
        }
        if (isset($_POST['order'][0]['dir'])){
            $columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
        }
        if (isset($_POST['search']['value'])){
            $searchValue = addslashes($_POST['search']['value']); // Search value
        }





        $search = trim($this->_request->getParam("search",""));
        $status = (int)trim($this->_request->getParam("status",-1));
        $group_type = (int)trim($this->_request->getParam("group_type",0));




        $searchQuery = " ";
//        if($searchValue != ''){
//            $searchQuery .= " and (emp_name like '%".$searchValue."%' or email like '%".$searchValue."%' or city like'%".$searchValue."%' ) ";
//        }

        $where_search = " 1=1 ";
        if ($search){
            $where_search .= " AND title like '%$search%'";
        }
        if ($group_type){
            $where_search .= " AND group_type = '{$group_type}'";
        }
        if ($status != -1){
            $where_search .= " AND enabled = '{$status}'";
        }



        $sql_count = "SELECT count(*) FROM addon_banner where {$where_search}";
        $list_count = $_general->excuteCodev2($sql_count);
        $totalRecordwithFilter = $totalRecords = (int)$list_count[0]['count(*)'];

        $page = trim($this->_request->getParam("page",""));
        if ($page=="all"){
            $limit = "";
        }else{
            $limit = " LIMIT {$row},{$rowperpage}";
        }

        $columnName = str_replace("p_","",$columnName);
        if ($columnIndex==0){
            $order = "order by id DESC";
        }else{
            $order = " order by ".$columnName." ".$columnSortOrder;
        }

        $sql_list = "SELECT * FROM addon_banner where {$where_search} {$order} {$limit} ";

        $list = $_general->excuteCodev2($sql_list);

        $data = array();

        if ($list){

            $stt=$row+1;
            foreach ($list as $val){
                $name_memberships_class = "";

                $image = "";
                if ($val['images']){
                    $image = '<img src="'.Globals::getBaseUrl().$val['images'].'" width="80" height="80" style="height:auto">';
                }
                $title = '<a href="/admin/banner/edit?id='.$val['id'].'" title="'.$val['title'].'">'.$val['title'].'</a>';

                if ($page=="all"){
                    if ($val['enabled']==1){
                        $status = "Hiển thị";
                    }else{
                        $status = "Tắt";
                    }
                    $title = $val['title'];
                }else{
                    if ($val['enabled']==1){

                        $status = '<input onchange="changeStatus('.$val["id"].',\''.md5("NewCenruryAbcdqwerBanner".$val['id']).'\',0)" class="status-changes" data-token="'.md5("LoyaltyAdminHNamAbcdqwerProducts".$val['id']).'" data-id="'.$val['id'].'" type="checkbox" id="status'.$val['id'].'" name="status'.$val['id'].'" value="1" checked data-bootstrap-switch>';
                    }else{
                        $status='<input onchange="changeStatus('.$val["id"].',\''.md5("NewCenruryAbcdqwerBanner".$val['id']).'\',1)" class="status-changes" type="checkbox" data-token="'.md5("LoyaltyAdminHNamAbcdqwerProducts".$val['id']).'" data-id="'.$val['id'].'" id="status'.$val['id'].'" name="status'.$val['id'].'" value="1" data-bootstrap-switch>';
                    }
                }
                $date_start = "";
                $date_end = "";
                if ($val['date_start']){
                    $date_start = date("H:i:s d-m-Y",strtotime($val['date_start']));
                }
                if ($val['date_end']){
                    $date_end = date("H:i:s d-m-Y",strtotime($val['date_end']));
                }
                $data[] = array(
                    "stt"=>$stt,
                    "p_images"=>$image,
                    "p_title"=>$title,
                    "p_group_type"=>$this->_bannersList[$val['group_type']],
                    "p_date_start"=>$date_start,
                    "p_date_end"=>$date_end,
                    "p_actived"=>$status,
                );
                $stt++;
            }
        }


        if ($page=="all"){
            $response = array(
                "data"=>$data
            );
        }else{
            $response = array(
                "draw" => intval($draw),
                "iTotalRecords" => $totalRecords,
                "iTotalDisplayRecords" => $totalRecordwithFilter,
                "aaData" => $data
            );
        }
        echo json_encode($response);
    }

}