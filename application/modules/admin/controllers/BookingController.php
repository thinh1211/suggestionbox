<?php
/* TODO: Add code here */
class Admin_BookingController extends Zend_Controller_Action
{
    private $menu = 'menu_booking';
    private $_identity;
    // private $_bookingList = array(
    //     "1"=> "Phòng 1",
    //     "2"=> "Phòng 2",
    // );

    public function init()
    {
        ini_set('display_errors', '1');
        BlockManager::setLayout('hnamtemplatecontent');
        $auth = Zend_Auth::getInstance();
        $identity = $auth->getIdentity();
        if(!is_null($identity) and count($identity) != 0) {
            $fullname = $identity->fullname?$identity->fullname:$identity->username;
            $this->view->fullname = $fullname;
        }else{
            $this->_redirect('/admin/home/login');
        }
        $this->_identity = (array) $auth->getIdentity();
        $this->view->menu_active = "booking";
    }

    public function indexAction() {
        // $data_pageshow= $this->_bookingList;
        // $this->view->data_pageshow=$data_pageshow;
    }

    public function listBookingAction(){
        
        $this->view->menu_sub_active = "list_booking";
//        css datatables
        $this->view->headLink()->appendStylesheet("/admin/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css");
        $this->view->headLink()->appendStylesheet("/admin/plugins/datatables-responsive/css/responsive.bootstrap4.min.css");
        $this->view->headLink()->appendStylesheet("/admin/plugins/datatables-buttons/css/buttons.bootstrap4.min.css");
//js datatables
        $this->view->inlineScript()->appendFile("/admin/plugins/datatables/jquery.dataTables.min.js");
        $this->view->inlineScript()->appendFile("/admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js");
        $this->view->inlineScript()->appendFile("/admin/plugins/datatables-responsive/js/dataTables.responsive.min.js");
        $this->view->inlineScript()->appendFile("/admin/plugins/datatables-responsive/js/responsive.bootstrap4.min.js");
        $this->view->inlineScript()->appendFile("/admin/plugins/datatables-buttons/js/dataTables.buttons.min.js");
        $this->view->inlineScript()->appendFile("/admin/plugins/datatables-buttons/js/buttons.bootstrap4.min.js");
        $this->view->inlineScript()->appendFile("/admin/plugins/jszip/jszip.min.js");
        $this->view->inlineScript()->appendFile("/admin/plugins/pdfmake/pdfmake.min.js");
        $this->view->inlineScript()->appendFile("/admin/plugins/pdfmake/vfs_fonts.js");
        $this->view->inlineScript()->appendFile("/admin/plugins/datatables-buttons/js/buttons.html5.min.js");
        $this->view->inlineScript()->appendFile("/admin/plugins/datatables-buttons/js/buttons.print.min.js");
        $this->view->inlineScript()->appendFile("/admin/plugins/datatables-buttons/js/buttons.colVis.min.js");
        $this->view->inlineScript()->appendFile("/admin/js/booking.js?v=".Globals::getVersion());
        // $data_pageshow= $this->_bookingList;
        // $this->view->data_pageshow=$data_pageshow;
    }
    
    public function ajaxListBookingAction(){
        
        $this->_helper->Layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $_general = Business_Addon_General::getInstance();
        $draw = 0;
        $row = 0;
        if (isset($_POST['start'])){
            $row = $_POST['start'];
        }
        if (isset($_POST['draw'])){
            $draw = $_POST['draw'];
        }

        if (isset($_POST['length'])){
            $rowperpage = $_POST['length']; // Rows display per page
        }
        $columnIndex = 0;
        $columnName = "";
        if (isset($_POST['order'][0]['column'])){
            $columnIndex = $_POST['order'][0]['column']; // Column index
            $columnName = $_POST['columns'][$columnIndex]['data']; // Column name
        }
        if (isset($_POST['order'][0]['dir'])){
            $columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
        }
        if (isset($_POST['search']['value'])){
            $searchValue = addslashes($_POST['search']['value']); // Search value
        }





        $search = trim($this->_request->getParam("search",""));
        // $status = (int)trim($this->_request->getParam("status",-1));
        // $parent_id= (int)trim($this->_request->getParam("parent_id",0));




        $searchQuery = " ";
       if($searchValue != ''){
           $searchQuery .= " and (emp_name like '%".$searchValue."%' or email like '%".$searchValue."%' or city like'%".$searchValue."%' ) ";
       }

        $where_search = " 1=1 ";
        if ($search){
            $where_search .= " AND title like '%$search%'";
        }
        // if ($parent_id){
        //     $where_search .= " AND parent_id = '{$parent_id}'";
        // }
        // if ($status != -1){
        //     $where_search .= " AND enabled = '{$status}'";
        // }



        $sql_count = "SELECT count(*) FROM addon_booking where {$where_search}";
        $list_count = $_general->excuteCodev2($sql_count);
        $totalRecordwithFilter = $totalRecords = (int)$list_count[0]['count(*)'];

        $page = trim($this->_request->getParam("page",""));
        if ($page=="all"){
            $limit = "";
        }else{
            $limit = " LIMIT {$row},{$rowperpage}";
        }

        $columnName = str_replace("p_","",$columnName);
        if ($columnIndex==0){
            $order = "order by id ";
        }else{
            $order = " order by ".$columnName." ".$columnSortOrder;
        }

        $sql_list = "SELECT * FROM addon_booking where {$where_search} {$order} {$limit} ";

        $list = $_general->excuteCodev2($sql_list);
        
        // echo "<pre>";
        // var_dump($list);
        // echo "</pre>";

        $data = array();

        if ($list){
            $stt=$row+1;
            foreach ($list as $val){
                $customername = '<a href="/admin/booking/edit?id='.$val['id'].'" customername="'.$val['customername'].'">'.$val['customername'].'</a>';
                // $customername = $val['customername'];
                $data[] = array(
                    "stt"=>$stt,
                    "p_customername"=>$customername,
                    "p_phone"=>$val["phone"],
                    // "p_dsMuaHang"=>$val["dsMuaHang"],
                );
                $stt++;
            }
        }


        if ($page=="all"){
            $response = array(
                "data"=>$data
            );
        }else{
            $response = array(
                "draw" => intval($draw),
                "iTotalRecords" => $totalRecords,
                "iTotalDisplayRecords" => $totalRecordwithFilter,
                "aaData" => $data
            );
        }
        // $this->viewParams['data'] = $data;
        echo json_encode($response);
    }

    public function editAction() {
        $this->view->menu_sub_active = "list_booking";
        $this->view->inlineScript()->appendFile("/admin/js/booking.js?v=".Globals::getVersion());
        $id = (int)$this->_request->getParam("id");
        $detail = array();
        if ($id){
            $__banner = Business_Addon_Booking::getInstance();
            $detail = $__banner->getDetail($id);
        }
        $this->view->detail = $detail;
        $this->view->token = Business_Addon_General::getInstance()->getToken();
    }

}

