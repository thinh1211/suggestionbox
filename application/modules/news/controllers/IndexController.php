<?php
class News_IndexController extends Zend_Controller_Action
{
    private $html = 0;
    private $menu_actived = 'trang-chu';
    public function init()
    {
        BlockManager::setLayout('no-sidebar');
        $this->view->menu_actived = $this->menu_actived;
    }

    public function indexAction()
    {
        if (Business_Addon_General::getInstance()->isLayoutV2()){

            $this->_helper->viewRenderer('v2-index');
            $this->view->noIndex = true;
            $this->view->js = array(
                'jquery' => Globals::getStaticUrl() . "v2/js/jquery-3.3.1.slim.min.js?v=" . Globals::getVersion(),
                'slick' => Globals::getStaticUrl() . "v2/vendor/slick/slick.min.js?v=" . Globals::getVersion(),
                'swiper' => Globals::getStaticUrl() . "v2/vendor/swiper/swiper-bundle.min.js?v=" . Globals::getVersion(),
                'main' => Globals::getStaticUrl() . "v2/js/home.js?v=" . Globals::getVersion(),
            );
            $this->view->css = array(
                'home' => "home.phtml",
                'slick' => "slick.phtml",
                'swiper' => "swiper.phtml",
            );
//            get list banner
            $__banner = Business_Addon_Banner::getInstance();
            
            $list_banner = $__banner->getList(2,1);
            $this->view->list_banner = $list_banner;

//            get list ưu đãi
            $__news = Business_Addon_News::getInstance();
            $list_offer = $__news->getList(4,6);
            $this->view->list_offer = $list_offer;
//            get list tin tức
            $list_news = $__news->getDetail(4);
            $this->view->list_news = $list_news;

            //get list suggestion
            $result = Business_Addon_Hnproducts::getInstance()->callGetRandomObjectsByThuongHieuChipPin(
                23803,24274,24500,24706,25004);

            if ($result && isset($result[0]['randomObjects'])) {
                $randomObjects = explode(',', $result[0]['randomObjects']);
                $list =[];

                foreach ($randomObjects as $value) {
                    $list[] = Business_Addon_Hnproducts::getInstance()->getList($value);
                }
            }
            
        
        $this->view->list_suggestion = $list;
            

        }else{
            $this->view->classBody = 'page-home';
            $this->view->noIndex = true;
            $this->view->js = array(
                'main' => Globals::getStaticUrl() . "backend/js/home.js?v=" . Globals::getVersion(),
            );

            
        }
        
    



    }
    public function aboutAction(){
        if (Business_Addon_General::getInstance()->isLayoutV2()){
            $this->_helper->viewRenderer('v2-about');
            $this->view->noIndex = true;
            $this->view->js = array(

                'main' => Globals::getStaticUrl() . "v2/js/about.js?v=" . Globals::getVersion(),
            );
            $this->view->css = array(
                'about' => "about.phtml",
            );
        }else{
            $this->view->classBody = 'page-about-p';
            $this->view->noIndex = true;
            $this->view->js = array(
                'main' => Globals::getStaticUrl() . "backend/js/about.js?v=" . Globals::getVersion(),
            );
        }

    }
    public function contactAction(){
        if (Business_Addon_General::getInstance()->isLayoutV2()){
            $this->_helper->viewRenderer('v2-contact');
            $this->view->noIndex = true;
            $this->view->js = array(

                'main' => Globals::getStaticUrl() . "v2/js/contact.js?v=" . Globals::getVersion(),
            );
            $this->view->css = array(
                'contact' => "contact.phtml",
            );
        }else{
            $this->view->classBody = 'page-contact-p';
            $this->view->noIndex = true;
        }
    }
    public function categoriesAction(){
        if (Business_Addon_General::getInstance()->isLayoutV2()){
            $this->_helper->viewRenderer('v2-categories');
            $this->view->noIndex = true;
            $this->view->js = array(

                'main' => Globals::getStaticUrl() . "v2/js/room.js?v=" . Globals::getVersion(),
            );
            $this->view->css = array(
                'room' => "room.phtml",
            );
        }else{
            $this->view->classBody = 'page-product-p';
            $this->view->noIndex = true;
            $this->view->js = array(
                'main' => Globals::getStaticUrl() . "backend/js/products.js?v=" . Globals::getVersion(),
            );
        }
    }
    // public function productDetailAction(){
    //     if (Business_Addon_General::getInstance()->isLayoutV2()){
    //         $this->_helper->viewRenderer('v2-product-detail');
    //         $this->view->noIndex = true;
    //         $this->view->js = array(
    //             'jquery' => Globals::getStaticUrl() . "v2/js/jquery-3.3.1.slim.min.js?v=" . Globals::getVersion(),
    //             'slick' => Globals::getStaticUrl() . "v2/vendor/slick/slick.min.js?v=" . Globals::getVersion(),
    //             'swiper' => Globals::getStaticUrl() . "v2/vendor/swiper/swiper-bundle.min.js?v=" . Globals::getVersion(),
    //             'main' => Globals::getStaticUrl() . "v2/js/room-detail.js?v=" . Globals::getVersion(),
    //         );
    //         $this->view->css = array(
    //             'detail' => "room-detail.phtml",
    //             'slick' => "slick.phtml",
    //             'swiper' => "swiper.phtml",
    //         );
    //     }else{
    //         $this->view->classBody = 'page-product-detail';
    //         $this->view->noIndex = true;
    //         $this->view->js = array(
    //             'main' => Globals::getStaticUrl() . "backend/js/detail.js?v=" . Globals::getVersion(),
    //         );
    //     }
    // }

    #region test
    public function productDetailAction(){
        if (Business_Addon_General::getInstance()->isLayoutV2()){
            $this->_helper->viewRenderer('v2-product-detail');
            $this->view->noIndex = true;
            $this->view->js = array(
                'jquery' => Globals::getStaticUrl() . "v2/js/jquery-3.3.1.slim.min.js?v=" . Globals::getVersion(),
                'slick' => Globals::getStaticUrl() . "v2/vendor/slick/slick.min.js?v=" . Globals::getVersion(),
                'swiper' => Globals::getStaticUrl() . "v2/vendor/swiper/swiper-bundle.min.js?v=" . Globals::getVersion(),
                'main' => Globals::getStaticUrl() . "v2/js/room-detail.js?v=" . Globals::getVersion(),
            );
            $this->view->css = array(
                'detail' => "room-detail.phtml",
                'slick' => "slick.phtml",
                'swiper' => "swiper.phtml",
            );
            $group_cate = 1;
            $itemid = $this->_request->getParam("itemid");
            $slug = $this->_request->getParam("slug");
            
            $detail = Business_Addon_Room::getInstance()->getRoomDetail($itemid,$group_cate);

            if (!$detail || $detail['enabled']!=1){
                header("location: /");
                die("1");
            }
            if ($detail['slug'] != $slug){
                $url = Globals::getBaseUrl()."room/".$detail['slug'].".".$detail['id'].".html";
                header("location: ".$url);
                die("2");
            }

            $this->view->detail = $detail;
            $parent_id = $detail['parent_id'];
            // $list_related = Business_Addon_News::getInstance()->getListRelated("*",$parent_id,$detail['id'],2,1);
            // if (!$list_related){
            //     $list_related = Business_Addon_News::getInstance()->getListRelated("*",0,$detail['id'],2,1);
            // }
            // $this->view->list_related = $list_related;
        }
        else{
            $this->view->classBody = 'page-product-detail';
            $this->view->noIndex = true;
            $this->view->js = array(
                'main' => Globals::getStaticUrl() . "backend/js/detail.js?v=" . Globals::getVersion(),
            );
        }
    }
    #endregion test

    public function blogAction(){
        if (Business_Addon_General::getInstance()->isLayoutV2()){
            $slug = $this->_request->getParam("slug");
            $this->_helper->viewRenderer('v2-blog');
            $this->view->noIndex = true;
            $this->view->js = array(

                'main' => Globals::getStaticUrl() . "v2/js/blog.js?v=" . Globals::getVersion(),
            );
            $actived = 0;
            $this->view->css = array(
                'blog' => "blog.phtml",
            );
            $data_news = array();
            $_list_cate_new = Business_Addon_Cate::getInstance()->getCateNews(0,2);
            if ($_list_cate_new){
                foreach ($_list_cate_new as $key=>$item){
                    if ($slug==$item['slug']){
                        $actived = $item['id'];
                    }
                }
                $list_id = implode(',',array_column($_list_cate_new,'id'));
                
                $searchInput ="";
                if (isset($_GET['searchInput'])) {
                    $searchInput = $_GET['searchInput'];
                    
                }
                // var_dump($searchInput);
                if (empty($searchInput)) {
                    if ($list_id){
                        $list_news = Business_Addon_News::getInstance()->getListNew("*",$list_id,0,1);
                        if ($list_news){
                            foreach ($list_news as $key=>$val){
                                $data_news[$val['parent_id']][] = $val;
                                $data_news[0][] = $val;
                            }
                        }
                    }
                    
                }
                 else {
                    if ($list_id){
                        $list_news = Business_Addon_News::getInstance()->getListNewByTitle("*",$list_id,0,1,$searchInput);
                        if ($list_news){
                            foreach ($list_news as $key=>$val){
                                $data_news[$val['parent_id']][] = $val;
                                $data_news[0][] = $val;
                            }
                        }
                    }
                }
                
                $result = Business_Addon_Hnproducts::getInstance()->callGetRandomObjectsByThuongHieuChipPin(
                    23803,24274,24500,24706,25004);

                if ($result && isset($result[0]['randomObjects'])) {
                    $randomObjects = explode(',', $result[0]['randomObjects']);
                    $list =[];

                    foreach ($randomObjects as $value) {
                        $list[] = Business_Addon_Hnproducts::getInstance()->getList($value);
                    }
                }
                if ($result && isset($result[0]['randomObjects'])) {
                    $randomObjects = explode(',', $result[0]['randomObjects']);
                    $count = count($randomObjects);
                    echo "Số lượng giá trị trong list: " . $count;
                }
                
            }
            $this->view->list_suggestion = $list;
            $this->view->list_cate = $_list_cate_new;
            $this->view->data_news = $data_news;
            $this->view->actived = $actived;
        }else{
            $this->view->classBody = 'page-blog';
            $this->view->noIndex = true;
        }
    }
    public function blogDetailAction(){
        if (Business_Addon_General::getInstance()->isLayoutV2()){
            $this->_helper->viewRenderer('v2-blog-detail'); //css
            $this->view->noIndex = true;
            $this->view->js = array(

                'main' => Globals::getStaticUrl() . "v2/js/blog.js?v=" . Globals::getVersion(),
            );
            $this->view->css = array(
                'blog' => "blog.phtml",
            );
            $itemid = $this->_request->getParam("itemid");
            $slug = $this->_request->getParam("slug");
            $detail = Business_Addon_News::getInstance()->getDetail($itemid);
            if (!$detail || $detail['enabled']!=1){
                header("location: /");
                die();
            }
            if ($detail['slug'] != $slug){
                $url = Globals::getBaseUrl()."news/".$detail['slug'].".".$detail['id'].".html";
                header("location: ".$url);
                die();
            }

            $this->view->detail = $detail;
            $parent_id = $detail['parent_id'];
            $list_related = Business_Addon_News::getInstance()->getListRelated("*",$parent_id,$detail['id'],2,1);
            if (!$list_related){
                $list_related = Business_Addon_News::getInstance()->getListRelated("*",0,$detail['id'],2,1);
            }
            $this->view->list_related = $list_related;
        }else{
            $this->view->classBody = 'page-blog-detail';
            $this->view->noIndex = true;
        }
    }
    public function searchAction(){
        $this->view->classBody = 'page-search';
        $this->view->noIndex = true;
        $this->view->js = array(
            'main' => Globals::getStaticUrl() . "backend/js/search.js?v=" . Globals::getVersion(),
        );
    }
    public function offerAction(){
        if (Business_Addon_General::getInstance()->isLayoutV2()){
            $this->_helper->viewRenderer('v2-offer');
            $this->view->noIndex = true; // không cho Search Engines tìm ra và đề cử trang web này khi user search "offer $proj_name" chẳng hạn!
            $this->view->js = array(

                'main' => Globals::getStaticUrl() . "v2/js/offer.js?v=" . Globals::getVersion(),
            );
            $this->view->css = array(
                'offer' => "offer.phtml",
            );
        }
    }

    public function bookingAction() {
        // $this->view->menu_sub_active = "list_booking";
        // $__booking = Business_Addon_Booking::getInstance();
        $__product = Business_Addon_Products::getInstance();
        // $list_cate = $__cate->getListCateGroup(2,false);
        $list_cate_product = $__product->getListForBooking(1,false);
        $this->view->headLink()->appendStylesheet("/admin/plugins/summernote/summernote-bs4.min.css");
        $this->view->inlineScript()->appendFile("/admin/plugins/summernote/summernote-bs4.min.js?v=".Globals::getVersion());
        $this->view->inlineScript()->appendFile("/admin/js/news.js?v=".Globals::getVersion());
        // $id = (int)$this->_request->getParam("id");
        $detail = array();
        // if ($id){
        //     $__news = Business_Addon_News::getInstance();
        //     $detail = $__news->getDetail($id);
        // }
        $this->view->list_cate_product = $list_cate_product;
        $this->view->detail = $detail;
        $this->view->token = Business_Addon_General::getInstance()->getToken();
    }

}