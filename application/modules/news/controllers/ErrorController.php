<?php
class News_ErrorController extends Zend_Controller_Action
{
    private $html = 0;
    private $menu_actived = 'thay-linh-kien';
    private $title = '';
    public function init()
    {
        $this->_helper->Layout()->disableLayout();
        $this->view->noIndex = true;
        $this->view->noCache = true;
        $module = $this->_request->getParam('module');
        header("HTTP/1.0 404 Not Found");
        $this->view->breadcrumbs = array();
        // var_dump($module);
        if ($module == 'hnamcare') {
            
            $layout = 'no-sidebar';
            BlockManager::setLayout($layout);
        }
        else {
            // $layout = 'no-sidebar';
            // BlockManager::setLayout($layout);
            die('');

        }
    }

    public function indexAction()
    {

    }
}