<?php
class News_SectionController extends Zend_Controller_Action
{

    public function init()
    {

    }

    public function headerAction()
    {
        if (Business_Addon_General::getInstance()->isLayoutV2()){
            $this->_helper->viewRenderer('v2-header');
        }
        $__cate = Business_Addon_Cate::getInstance();
        $menu_room = $__cate->getCateByParentId(0,1);
        $this->view->menu_room = $menu_room;

        $urlLink = "newcentury.xyz" . $_SERVER['REQUEST_URI'];
        //lưu hành trình user
        if (isset($_COOKIE['user-trip'])) {
            if (strpos($urlLink, 'newcentury.xyz/cart') !== false || strpos($urlLink, 'newcentury.xyz//room') !== false
            || strpos($urlLink, 'newcentury.xyz/room') !== false) {
           
                $userTripValue = $_COOKIE['user-trip'];
                $cookieValues = explode('|', $userTripValue);
                $id = $cookieValues[0];
                $browser = $_SERVER['HTTP_USER_AGENT'];
                $ip = $_SERVER['REMOTE_ADDR'];

                $currentTime = microtime(true);
                $formattedTime = date('Y-m-d H:i:s', $currentTime);
                $timeStamp = $formattedTime;
                $value = $id . '|' . $browser . '|' . $ip . '|' . $urlLink . '|' . $timeStamp;
                setcookie('user-trip', $value, time() + 604800, '/', 'newcentury.xyz');

                $data = array(
                    'id' => $id,
                    'browser' => $browser,
                    'ip' => $ip,
                    'urlLink' => $urlLink,
                    'timestamp' => $timeStamp,
                );
                Business_Addon_General::getInstance()->insertDB("addon_user_trip",$data);
            }  
        } else {
                $id = time() . mt_rand(0, 9999);
                $browser = $_SERVER['HTTP_USER_AGENT'];
                $ip = $_SERVER['REMOTE_ADDR'];
                $currentTime = microtime(true);
                $formattedTime = date('Y-m-d H:i:s', $currentTime);
                $timeStamp = $formattedTime;

                $value = $id . '|' . $browser . '|' . $ip . '|' . $urlLink . '|' . $timeStamp;
                $data = array(
                    'id' => $id,
                    'browser' => $browser,
                    'ip' => $ip,
                    'urlLink' => $urlLink,
                    'timestamp' => $timeStamp,
                );
                Business_Addon_General::getInstance()->insertDB("addon_user_trip",$data);
            }
    }

    public function footerAction()
    {
        if (Business_Addon_General::getInstance()->isLayoutV2()){
            $this->_helper->viewRenderer('v2-footer');
            $this->view->inlineScript()->appendFile(Globals::getStaticUrl() . "v2/js/script.js?v=" . Globals::getVersion());
            if (!empty($this->view->js)) {
                foreach($this->view->js as $key=>$js) {
                    if ($key == 'swiper-bundle') continue;
                    $this->view->inlineScript()->appendFile($js);
                }
            }
        }else{
            $this->view->inlineScript()->appendFile(Globals::getStaticUrl() . "backend/js/common.js?v=" . Globals::getVersion());

            if (!empty($this->view->js)) {
                foreach($this->view->js as $key=>$js) {
                    if ($key == 'swiper-bundle') continue;
                    $this->view->inlineScript()->appendFile($js);
                }
            }
        }

    }
}