<?php
class News_CartController extends Zend_Controller_Action {
    public function init()
    {
         ini_set('display_errors', '1');
         BlockManager::setLayout('no-sidebar');
         $this->view->menu_active = "cart";
    }


    public function cartAction() {
        
        if (Business_Addon_General::getInstance()->isLayoutV2()){
            
             $this->view->noIndex = true;
             $this->view->js = array(
                'main' => Globals::getStaticUrl() . "v2/js/cart.js?v=" . Globals::getVersion(),
                
            );    
        }else{
            $this->view->classBody = 'page-home';
            $this->view->noIndex = true;
            $this->view->js = array(
                'main' => Globals::getStaticUrl() . "backend/js/home.js?v=" . Globals::getVersion(),
            );
        }
        if (isset($_COOKIE['thong-tin-dat-phong'])) {
            $cookie = $_COOKIE['thong-tin-dat-phong'];
            $decodedData = urldecode($cookie);
            $arrayData = json_decode($decodedData, true);

            $resultArray = []; // Mảng để lưu dữ liệu từ cơ sở dữ liệu

            
            foreach ($arrayData as $data) {
                $id = $data['id'];
                $quantity = $data['quantity'];

                $dataFromDB = Business_Addon_Cate::getInstance()->FindAllByID($id); 
                if ($dataFromDB) {
                    $dataFromDB[0]['quantity'] = $quantity;
                    $resultArray[] = $dataFromDB;
                }
            }
            echo "<pre>";
            var_dump($resultArray);
            echo "</pre>";
            
            $itemCount = [];
            foreach ($resultArray as $data) {
                $id = $data[0]['id'];

                if (isset($itemCount[$id])) {
                    $itemCount[$id]++;
                } else {
                    $itemCount[$id] = 1;
                }
            }
        

        $this->view->arrayData = $resultArray; // Gửi chuỗi JSON xuống Views
        $this->view->itemCount = $itemCount;
            
        }    

        
        // $this->view->token = Business_Addon_General::getInstance()->getToken();
    }

    

    
}

?>

