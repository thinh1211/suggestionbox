<?php
class News_BookingController extends Zend_Controller_Action {

    // private $menu = 'menu_booking';
    // private $menu = 'menu_news';

    public function init()
    {
        ini_set('display_errors', '1');
         BlockManager::setLayout('no-sidebar');
         $this->view->menu_active = "booking";
    }

    public function indexAction()
    {

    }

    public function bookingAction() {
        if (Business_Addon_General::getInstance()->isLayoutV2()){
             $this->view->noIndex = true;
             $this->view->js = array(
                'main' => Globals::getStaticUrl() . "v2/js/booking.js?v=" . Globals::getVersion(),

            );       
        }else{
            $this->view->classBody = 'page-home';
            $this->view->noIndex = true;
            $this->view->js = array(
                'main' => Globals::getStaticUrl() . "backend/js/home.js?v=" . Globals::getVersion(),
            );
        }

        $this->view->menu_sub_active = "list_booking";
        $__product = Business_Addon_Products::getInstance();
        $list_cate_product = $__product->getListForBooking();
        // $this->view->headLink()->appendStylesheet("/admin/plugins/summernote/summernote-bs4.min.css");
        // $this->view->inlineScript()->appendFile("/admin/plugins/summernote/summernote-bs4.min.js?v=".Globals::getVersion());
        // $this->view->inlineScript()->appendFile("/admin/js/news.js?v=".Globals::getVersion());
        // die('he');
        $id = (int)$this->_request->getParam("id");
        // $detail = array();
        // if ($id){
        //     $__booking = Business_Addon_Booking::getInstance();
        //     $detail = $__booking->getDetail($id);
        // }
        $this->view->list_cate_product = $list_cate_product;
        // $this->view->detail = $detail;
        $this->view->token = Business_Addon_General::getInstance()->getToken();
    }

    public function bookingSuccessAction() {
        if (Business_Addon_General::getInstance()->isLayoutV2()){
            $this->view->noIndex = true;
            $this->view->js = array(
               'main' => Globals::getStaticUrl() . "v2/js/booking.js?v=" . Globals::getVersion(),

           );
        }else{
            $this->view->classBody = 'page-home';
            $this->view->noIndex = true;
            $this->view->js = array(
                'main' => Globals::getStaticUrl() . "backend/js/home.js?v=" . Globals::getVersion(),
            );
        }
        $this->view->menu_sub_active = "booking_success";
        $this->view->token = Business_Addon_General::getInstance()->getToken();
    }
}

?>