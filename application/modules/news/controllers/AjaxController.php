<?php
class News_AjaxController extends Zend_Controller_Action
{
    private $_prefix_cache = 'News_AjaxController::';
    public function init()
    {
        $this->_helper->Layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->view->noIndex = true;
    }



    function wrap($fontSize, $angle, $fontFace, $string, $width){

        $ret = "";

        $arr = explode(' ', $string);

        foreach ( $arr as $word ){
            $teststring = $ret.' '.$word;
            $testbox = imagettfbbox($fontSize, $angle, $fontFace, $teststring);
            if ( $testbox[2] > $width ){
                $ret.=($ret==""?"":"\n").$word;
            } else {
                $ret.=($ret==""?"":' ').$word;
            }
        }

        return $ret;
    }

    private function properText($text){

        // Convert UTF-8 string to HTML entities
        $text = mb_convert_encoding($text, 'HTML-ENTITIES',"UTF-8");
        // Convert HTML entities into ISO-8859-1
        $text = html_entity_decode($text,ENT_NOQUOTES, "ISO-8859-1");
        // Convert characters > 127 into their hexidecimal equivalents
        $out = "";
        for($i = 0; $i < strlen($text); $i++) {
            $letter = $text[$i];
            $num = ord($letter);
            if($num>127) {
                $out .= "&#$num;";
            } else {
                $out .=  $letter;
            }
        }

        return $out;

    }

    public function ajaxSearchingAction() {     
        if (Business_Addon_General::getInstance()->isLayoutV2()){
            $slug = $this->_request->getParam("slug");
            $this->_helper->viewRenderer('v2-blog');
            $this->view->noIndex = true;
            $this->view->js = array(

                'main' => Globals::getStaticUrl() . "v2/js/blog.js?v=" . Globals::getVersion(),
            );
            $actived = 0;
            $this->view->css = array(
                'blog' => "blog.phtml",
            );
        
            $searchInput = $_POST['search'];
            $_list_cate_new = Business_Addon_Cate::getInstance()->getCateNews(0,2);
                if ($_list_cate_new){
                    foreach ($_list_cate_new as $key=>$item){
                        if ($slug==$item['slug']){
                            $actived = $item['id'];
                        }
                    }
                    $list_id = implode(',',array_column($_list_cate_new,'id'));
                    $data_news = array();
                    if (!empty($searchInput)) {
                        // $list_news = Business_Addon_News::getInstance()->getListNewByTitle("*",$list_id,0,1,$searchInput);
                        if ($list_id){
                                    $list_news = Business_Addon_News::getInstance()->getListNewByTitle("*",$list_id,0,1,$searchInput);
                                    if ($list_news){
                                        foreach ($list_news as $key=>$val){
                                            $data_news[$val['parent_id']][] = $val;
                                            $data_news[0][] = $val;
                                        }
                                    }
                                }
                        // echo "<pre>";
                        // var_dump($data_news);
                        // die();
                        $cache = GlobalCache::getCacheInstance('ws');
                        $cache->flushAll();  
                        echo json_encode($data_news);
                        // echo "test";
                        die();
                    }

                }
            }else{
                $this->view->classBody = 'page-blog';
                $this->view->noIndex = true;
            }
    }

    // public function updateTotalAction(){
    //     if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    //         $total = $_POST['total'];
    //         $rowTotal = $_POST['rowTotal'];
            
    //         $total = $total * $rowTotal;
    //         // Cập nhật biến $total và $rowTotal ở đây (ví dụ: gán giá trị cho biến $total và $rowTotal)
          
    //         // Gán biến $total là một cookie
    //         if (isset($_COOKIE['thong-tin-dat-phong'])) {
    //             setcookie('total', $total, time() + 3600, '/');
    //             setcookie('row-total', $rowTotal, time() + 3600, '/')
    //         }
    //         // Trả về phản hồi cho yêu cầu Ajax (nếu cần thiết)
    //         $response = array('success' => true);
    //         echo json_encode($response);
    //       }
    // }

    public function increaseDecreaseAction() {
        $price = $_POST['price'];
        
        $total = $_COOKIE['total'];
        $action = $_POST['action'];
        echo "<pre>";
        var_dump($action);
        echo "</pre>";
        if (isset($_POST['action'])) {
            $action = $_POST['action'];
        
            // Kiểm tra giá trị của action
            if ($action === 'increment') {         
                    $total = $total + $price ;     
                    setcookie('total', $total , time() + 3600, '/');

            } elseif ($action === 'decrement') {                    
                    $total = $total - $price ;
                    setcookie('total', $total , time() + 3600, '/');
                }
            } 
               
        
        // Trả về kết quả dưới dạng JSON
        $response = array(
        'success' => true,
        'total' => $total
        );
        echo json_encode($response);
    }

    public function submitBookingAction()
    {
        $id  = (int)$this->_request->getParam('id');
        $token  = $this->_request->getParam('token');
        $customername  = addslashes($this->_request->getParam('customername'));
        $phone  = addslashes($this->_request->getParam('phone'));
        
        $_ztoken = Business_Addon_General::getInstance()->checkToken($token);
        
        try{
            if ($_ztoken){
                if (!$customername) {
                    echo json_encode(array('msg' => 'Vui lòng nhập tên', 'field' => 'inputCustomerName'));
                    die();
                }
                if (!$phone) {
                    echo json_encode(array('msg' => 'Vui lòng nhập sđt', 'field' => 'inputPhone'));
                    die();
                }

                // $slug = Business_Addon_General::getInstance()->slugString($title);
                $data = array(
                    'customername' => $customername,
                    'phone' => $phone,
                );

                    $lastID = Business_Addon_General::getInstance()->insertDB("addon_booking",$data);
                    $data_res = array(
                        "msg" => "Đặt phòng thành công",
                        "url"=>"/news/booking/booking-success",
                        "reloads"=>true
                    );
                // }
                $cache = GlobalCache::getCacheInstance('ws');
                $cache->flushAll();

                echo json_encode($data_res);
                
                die();
            }else{
                $data_res = array(
                    "msg" => "Có lỗi xảy ra. Vui lòng thử lại 1",
                    "reloads"=>true
                );
                echo json_encode($data_res);
                die('aaa');
            }
        }catch (Exception $e){
            $data_res = array(
                "msg" => "Có lỗi xảy ra. Vui lòng thử lại 2",
                "reloads"=>true
            );
            echo json_encode($data_res);
            die('bbb');
        }
    }

    public function clearCookie() {
        
        die($_COOKIE);
         
    }

    public function shoppingAction()
    {
        $id  = (int)$this->_request->getParam('id');
        $customername  = addslashes($this->_request->getParam('customername'));
        $phone  = addslashes($this->_request->getParam('phone'));
        if (isset($_POST['total'])) {
            $total = $_POST['total'];
        } else {
            die($total);
        }
        $arrayData = json_decode($_POST['arrayData'], true);
        $dsMuaHang = (string) json_encode($arrayData);

        // echo '<pre>';
        // var_dump($dsMuaHang);
        // echo '</pre>';
        try{
            
            // if ($_ztoken){
                

                if (!$customername) {
                    echo json_encode(array('msg' => 'Vui lòng nhập tên', 'field' => 'inputCustomerName'));
                    die();
                }
                if (!$phone) {
                    echo json_encode(array('msg' => 'Vui lòng nhập sđt', 'field' => 'inputPhone'));
                    die();
                }
                // if (!$dsMuaHang){
                //     echo json_encode(array('msg' => 'Vui lòng mua hàng để được thanh toán'));
                //     die();
                // }
                // $slug = Business_Addon_General::getInstance()->slugString($title);
                $data = array(
                    'customername' => $customername,
                    'phone' => $phone,
                    'dsMuaHang' =>$dsMuaHang,
                );

                    $lastID = Business_Addon_General::getInstance()->insertDB("addon_booking",$data);
                    $data_res = array(
                        "msg" => "Đặt phòng thành công",
                        "url"=>"/news/booking/booking-success",
                        "reloads"=>true
                    );
                // }
                // if (isset($_COOKIE['thong-tin-dat-phong'])) {
                //     unset($_COOKIE['thong-tin-dat-phong']);
                //     setcookie('thong-tin-dat-phong', '', time() - 3600);
                // }
                                
                
                
        
                $cache = GlobalCache::getCacheInstance('ws');
                $cache->flushAll();
                echo json_encode($data_res);

                
                // echo "<pre>";
                // var_dump($_COOKIE);
                // echo "</pre>";


                die();
            // }else{
            //     $data_res = array(
            //         "msg" => "Có lỗi xảy ra. Vui lòng thử lại 1",
            //         "reloads"=>true
            //     );
            //     echo json_encode($data_res);
            //     die('aaa');
            // }
        }catch (Exception $e){
            $data_res = array(
                "msg" => "Có lỗi xảy ra. Vui lòng thử lại 2",
                "reloads"=>true
            );
            echo json_encode($data_res);
            die('bbb');
        }
    }

}

